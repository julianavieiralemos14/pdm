import { StyleSheet, Text, View, Button, TouchableOpacity } from "react-native";

function QuintaTela() {
  return (
    <View style={styles.container}>
      <TouchableOpacity>
      <Text>Clique aqui</Text>
      </TouchableOpacity>
    </View>
  );
}

export default QuintaTela;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: 'pink',
  },
});
