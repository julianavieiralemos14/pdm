import { StyleSheet, Text, View } from "react-native";

function QuartaTela() {
  return (
    <View style={styles.container}>
      <Text>Hello World!</Text>
      <Text>Olá Mundo!</Text>
      <Text>Hallo Welt!</Text>
      <Text>Bonjour le monde!</Text>
    </View>
  );
}

export default QuartaTela;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: 'pink',
  },
});
